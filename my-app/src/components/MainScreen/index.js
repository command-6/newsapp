import React from 'react';

import './index.css';
import Category from '../category';
import AddForm from "../AddForm";
import { Link } from 'react-router-dom';
import { api } from '../Api/api';


class Mainscreen extends React.Component {
  constructor() {
    super();
    this.state = {
      user: '',
      categories: [],
      adding: false,
      open: false,
      opendName: '',
      draggable: "true",
    }
    this.handleDragSort = this.handleDragSort.bind(this);
  }

  componentDidMount() {
    if (api.currentUser()) {
      let categories = api.getUserCategories();
      if (categories) {
        this.setState({ categories: categories });
      }
    }
  }

  addCategory = () => {
    this.setState({ adding: true });
  }

  setCategoryName = (name, description) => {
    api.addUserCategory(name, description);
    let categories = api.getUserCategories();
    this.setState({ categories, adding: false });
  }

  back = ()=>{
    this.setState({ adding: false });
  }

  deleteCategory = (name) => {
    api.removeUserCategory(name);
    let categories = api.getUserCategories();
    this.setState({ categories });
  }

  handleDragSort(itemPlaced) {
    let itemTaken = localStorage.getItem('dragTake');
    let categories = this.state.categories;
    let arr = categories;

    itemTaken = Number(itemTaken);
    itemPlaced = Number(itemPlaced);

    while (itemTaken - itemPlaced !== 0) {
      let diff = itemPlaced - itemTaken;
      if (diff > 0) {
        let tmp = arr[itemTaken];
        arr[itemTaken] = arr[itemTaken + 1];
        arr[itemTaken + 1] = tmp;
        itemTaken++;
      } else {
        let tmp = arr[itemTaken];
        arr[itemTaken] = arr[itemTaken - 1];
        arr[itemTaken - 1] = tmp;
        itemTaken--;
      }
    }
    api.dragDropSort(arr);
    localStorage.removeItem('dragPlace');
    this.setState({ categories: arr });
  }

  render() {
    let screen = null;
    if (this.state.adding) {
      screen = <AddForm props={this.setCategoryName} back={this.back}/>
    } else {
      let arr = this.state.categories.map((name, i) =>
        <Link to='/category' key={name} >
          <Category
            draggable={this.state.draggable}
            title={name}
            logo
            description="Все о мире IT"
            key={name}
            id={i}
            deleteCategory={this.deleteCategory}
            onDrop={this.handleDragSort}
          />
        </Link>);
      arr.unshift(<Category title="Добавить" className='add-category' newCategory={this.addCategory} key="Add Category" />);
      screen = arr;
    }

    return (
      <div className="main_screen">
        {screen}
      </div>
    )
  }

}

// ========================================

export default Mainscreen;
