import React from 'react';
import {
  Link,
} from "react-router-dom";
import './index.css';
import { ReactComponent as Logo } from './logo.svg';



function Footer() {
  return (
      <div className="footer">
        <div className="footer__container">
          <Link to="/" className="link">
            <div className='logo-container'>
              <div className='logo'>
                  <Logo />
              </div>
            </div>
          </Link>
          <div className="devs">
            <span>Made by:</span>
            <a href="https://github.com/Igor-Polonskiy" target="_blank" rel="noopener noreferrer" className="dev-name">Igor</a>
            <a href="https://github.com/Perchishka" target="_blank" rel="noopener noreferrer" className="dev-name">Elena</a>
            <a href="https://github.com/shameondev" target="_blank" rel="noopener noreferrer" className="dev-name">Daniil</a>
          </div>
        </div>
      </div>
  )
}

// ========================================

export default Footer;
