import React from 'react';
import './index.css';

class Defaultcategory extends React.Component {
  constructor() {
    super();
    this.state = {
      categoryName: '',
      key: '',
      deletebutton: false,
      deleteclass: 'deleteclass'
    }
    this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount() {
    let firstletter = this.props.title.split('')[0].toUpperCase();
    this.setState({
      categoryName: this.props.title,
      letter: firstletter,
    });
  }

  handleClick = () => {
    if (this.state.categoryName === 'Добавить') {
      this.props.newCategory();
    } else {
      localStorage.setItem('openedCategory', this.state.categoryName);
    }
  }

  render() {
    let classes = '';
    classes = 'category__item';
    let selectLogo = (props) => {
     return (
        <div>
          <div className='categoryico'>{this.state.letter}</div>
        </div>)
    }
    return (
      <div
        className={classes}
        onClick={this.handleClick}
      >
        <div className="category__icon">
          {selectLogo(this.props.className)}
        </div>
        <div className="category__title">
          <span>{this.props.title}</span>
        </div>
        <div className="category__description">
          {this.props.description}
        </div>
      </div>
    )
  }

}
export default Defaultcategory;
