import React, { useState } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import './index.css';
import Header from '../header';
import Footer from '../footer';
import MainScreen from '../MainScreen';
import OpenedCategory from "../OpenedCategory";
import { Login } from "../loginForm/index";
import Default from "../Default";
import { api } from "../Api/api";
import Defcat1 from '../defcat1';
import Defcat2 from '../defcat2';
import Defcat3 from '../defcat3';

function App() {  
  const [username, setUsername] = useState(api.currentUser()?.username);
  const login = () => setUsername(api.currentUser()?.username);
  const logout = () => {
    setUsername(null);
    api.logout();
  }


  return (
    <Router>
      <Header username={username} logout={logout} />
      <div className="screen container">
        <Switch>
          <Route path='/' exact>
            <Default />
          </Route>
          <Route path="/default" >
            <MainScreen />
          </Route>
          <Route path="/login" component={Login} >
            <Login  onLogin={login} />
          </Route>
          <Route path="/category">
            <OpenedCategory />
          </Route>
          <Route path='/defcat1'>
            <Defcat1 />
          </Route>
          <Route path='/defcat2'>
            <Defcat2 />
          </Route>
          <Route path='/defcat3'>
            <Defcat3 />
          </Route>
        </Switch>
      </div>
      <Footer />
    </Router>
  )

}

// ========================================

export default App;
