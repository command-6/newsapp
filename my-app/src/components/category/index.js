import React from 'react';
import './index.css';
import logoAdd from './add.svg';
import { api } from '../Api/api';




class Category extends React.Component {
  constructor() {
    super();
    this.state = {
      categoryName: '',
      description:'',
      key: '',
      deletebutton: false,
      deleteclass: 'deleteclass',
      letter: ""

    }
    this.handleClick = this.handleClick.bind(this);
  }

  dragStart = e => {
    localStorage.setItem('dragTake', this.props.id);
  }

  dragOver = e => {
    e.preventDefault();
  }

  handleDrop = e => {
    e.preventDefault();
    if (this.props.id !== undefined) {
      this.props.onDrop(this.props.id);
    }
  }

  componentDidMount() {
    let firstletter = this.props.title.split('')[0].toUpperCase();
    let description =  api.getDescription(this.props.title);
    this.setState({
      categoryName: this.props.title,
      description: description,
      letter: firstletter,
    });
  }


  handleClick = () => {
    if (this.state.categoryName === 'Добавить') {
      this.props.newCategory();
    } else {
      localStorage.setItem('openedCategory', this.state.categoryName);
    }
  }
  handleMousEnter = () => {
    if (this.state.categoryName !== 'Add Category') {
      this.setState({ deleteclass: 'deleteclass show' });
    }

  }
  handleMouseLeave = () => {
    this.setState({ deleteclass: 'deleteclass' })
  }
  handleDelete = (e) => {
    e.stopPropagation();
    e.preventDefault();
    this.props.deleteCategory(this.state.categoryName);
  }

  render() {
    let classes = '';
    if (this.props.className) {
      classes = `category__item ${this.props.className}`;
    } else {
      classes = 'category__item';
    }
    let selectLogo = (props) => {
      if (props) {
        return <img src={logoAdd} alt='logoAdd'/>
      } else return (
        <div>
          <button
            className={this.state.deleteclass}
            onClick={this.handleDelete}>
            x
          </button>
          <div className='categoryico'>{this.state.letter}</div>
        </div>
      )
    }
    return (
      <div
        onDrop={this.handleDrop}
        onDragOver={this.dragOver}
        onDragStart={this.dragStart}
        draggable={this.props.draggable}
        className={classes}
        onClick={this.handleClick}
        onMouseEnter={this.handleMousEnter}
        onMouseLeave={this.handleMouseLeave}>
        <div className="category__icon">
          {selectLogo(this.props.className)}
        </div>
        <div className="description__wrapper">
          <div className="category__title">
            <span>{this.props.title}</span>
          </div>
          <div className="category__description">
            {this.state.description}
          </div>
        </div>
      </div>
    )
  }
}

// ========================================

export default Category;
