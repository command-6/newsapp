import React from 'react';
import './index.css';
import AddSorceForm from "../AddSorceForm";
import Sorce from '../sorce';
import {api} from '../Api/api';




class OpenedCategory extends React.Component {
  constructor() {
    super();
    this.state = {
      sorces: [],
      name: '',
      adding: false,
    }
  }
  setSorceyName = (name) => {
    let sorces = this.state.sorces;
    sorces.push(name);
    this.setState({
      sorces: sorces,
      adding: false
    });
    api.addCategorySourse(this.state.name, name);

  }
  componentDidMount() {
    let categoryName = localStorage.getItem('openedCategory');
    if(categoryName === ''){}
    this.setState({ name: categoryName });
    let sorces = api.getCategorySources(categoryName);
    if (sorces) {
      this.setState({ sorces: sorces});
    }
  }
  handlClick = () => {
    this.setState({ adding: true })
  }

  handleDeleteElement = (sorce) => {
    let categoryName = localStorage.getItem('openedCategory');
    let sorces = this.state.sorces;
    sorces = sorces.filter((item) => item !== sorce);
    this.setState({ sorces: sorces });
    api.removeCategorySourse(categoryName, sorce);
  }

  render() {
    let screen = null;
    if (this.state.adding) {
      screen = <AddSorceForm props={this.setSorceyName} sorses={this.state.sorces}/>
    } else {
      let arr = this.state.sorces;
      if (arr) {
        screen = arr.map((sorce) => <Sorce sorce={sorce} className="category__item" key={sorce} delete={this.handleDeleteElement} />);
      }
    }

     const handleButtonShow = () => {
      if (this.state.adding) {
        return;
      } else {
      return (
        <div className='button__container'>
          <button className="button__black" onClick={this.handlClick} >Добавить источник</button>
        </div>
      );
      }
    }


    return (
      <div>
      <div className='flex cat_title'>{this.state.name}</div>
        {handleButtonShow()}
        <div className='flex category__list'>
          {screen}
        </div>
      </div>
    )
  }

}

// ========================================

export default OpenedCategory;
