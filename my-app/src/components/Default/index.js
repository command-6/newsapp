import React from "react";
import Defaultcategory from '../defaultcategory';
import './index.css';
import { Link } from "react-router-dom";


function Default() {
    return (
        <div className="categories__list">
            <div className="popular">
                <h3>Подборки от разработчиков</h3>
                <div className='pepole'>
                <Link to='/defcat1'  ><Defaultcategory title='Технологии' description="И как в них не заблудиться" /></Link>
                <Link to='/defcat2'  ><Defaultcategory title='Новости' description="Самые свежие и горячие" /></Link>
                <Link to='/defcat3'  > <Defaultcategory title='Искусство' description="Современное и не очень" /></Link>
                </div>
            </div>
        </div>
    )
}

export default Default;