export const api = {
    register: ({
        email,
        username,
        password
    }) => {
        const users = load();
        if(users.filter((u)=>u.email === email).length > 0 || users.filter((u)=>u.username === username).length >0){
            return "Данный пользователь уже зарегестрирован";
        }
        setUser({
            email,
            username,
            password,
            loggedIn: true
        });
        return "success";
    },
    login: ({
        username,
        password
    }) => {
        const user = getUser(username);
        if (!user) {
            return false;
        }
        if (user.password !== password) {
            return false;
        }
        setUser({
            username,
            loggedIn: true
        });
        return true;
    },
    logout: () => {
        setUser({
            ...getLoggedInUser(),
            loggedIn: false
        });
    },
    currentUser: () => getLoggedInUser(),

    addUserCategory: (category, description) => {
        const user = getLoggedInUser();
        let currentCategories = [];
        if (!user) {
            return;
        }
        if (user.categories) {
            currentCategories = user.categories;
        }
        currentCategories.push({
            category: category,
            description: description,
            sorces: []
        });
        setUser({
            username: user.username,
            categories: currentCategories
        });
    },

    getDescription: (title) => {
        const user = getLoggedInUser();
        let currentCategories = [];
        let description = ''
        if (!user) {
            return;
        } else
            currentCategories = user.categories;
        if (currentCategories) {
            currentCategories.forEach((item) => {
                if (item.category === title) {
                    description = item.description;
                }
            })
        }
        return description;
    },

    removeUserCategory: (category) => {
        const user = getLoggedInUser();
        if (!user) {
            return;
        }
        const currentCategories = user.categories || [];
        const newCategories = currentCategories.filter((item) => item.category !== category);
        setUser({
            username: user.username,
            categories: newCategories
        });
    },

    dragDropSort: (arr) => {
        const user = getLoggedInUser();
        let newCategories = [];
        for (let i = 0; i < arr.length; i++) {
            newCategories[i] = (user.categories.filter((item) => item.category === arr[i]))[0];
        }
        setUser({
            username: user.username,
            categories: newCategories
        });
    },

    getUserCategories: () => {
        const user = getLoggedInUser();
        let categories = [];
        if (!user) {
            categories = [];
        } else if (user.categories) {
            user.categories.forEach(element => {
                categories.push(element.category);
            });
        } else categories = [];
        return categories;
    },



    addCategorySourse: (name, source) => {
        const user = getLoggedInUser();

        user.categories.forEach((cat)=>{
            if(cat.category===name){
                cat.sorces.push(source);
                setUser({
                    username: user.username,
                    categories: user.categories
                });
            }
        })

    },

    removeCategorySourse: (name, source) => {
        const user = getLoggedInUser();

        user.categories.forEach((cat)=>{
            if(cat.category===name){
                cat.sorces=cat.sorces.filter((item) => item !== source);
                setUser({
                    username: user.username,
                    categories: user.categories
                });
            }
        })
    },

    getCategorySources: (name) => {
        const user = getLoggedInUser();
        let sorces = [];
        user.categories.forEach((cat)=>{
            if(cat.category===name){
                sorces=cat.sorces
            }
        })
        return sorces;
    }
}

function getUser(_username) {
    const users = load();
    if (!users) {
        return null;
    }
    return users.filter(({
        username
    }) => username === _username)[0];
}

function setUser(user) {
    if (!user) {
        return;
    }
    if (!user.username) {
        return;
    }
    const users = load();
    if (!users) {
        save([user]);
        return;
    }
    const existingUser = users.filter(({
        username
    }) => username === user.username);
    if (existingUser.length === 0) {
        save([...users, user]);
        return;
    }
    const updatedUser = {
        ...existingUser[0],
        ...user
    };
    let otherUsers = users.filter(({
        username
    }) => username !== user.username);
    if (user.loggedIn) {
        otherUsers = otherUsers.map((_) => ({
            ..._,
            loggedIn: false
        }));
    }
    save([...otherUsers, updatedUser]);
}

function getLoggedInUser() {
    const user = load()?.filter(({
        loggedIn
    }) => loggedIn)[0];
    return user;
}

function load() {
    const data = localStorage.getItem("users");
    if (!data) {
        return [];
    }
    return JSON.parse(data);
}


function save(data) {
    localStorage.setItem("users", JSON.stringify(data));
}
