import React, {useState} from 'react';
import {
  Link,
} from "react-router-dom";
import './index.css';
import { ReactComponent as Logo } from './logo.svg';



function Header({ username, logout }) {
  let [categoryPath, setCategoryPath] = useState('');
  let [animate, setAnimate] = useState(false);
  let path='';
  if (username){
    path = '/default';
  } else path = '/';
  
  setCategoryPath = () => {
      categoryPath = document.location.pathname;
      return categoryPath;
  }
  const handleTitle = () => {
    if (username) {
      return (
        <div>
          <div className="background-circle"></div>
          <div className="above">Добро пожаловать,<br/><span className="nickname">{username}</span></div>
        </div>
      )
    } else {
      return (
        <div>
          <div className="background-circle"></div>
          <div className="above">Любимые ресурсы<br/><span className="nickname">собраны вместе</span></div>
          <div className="title-small above">Создавайте, храните и делитесь своими подборками с другими людьми.</div>
        </div>
      )
    }
  }
  return (
    <div className="header">
      <div className="header_container">
          <Link to={path}  onClick={() => setAnimate(true)} onAnimationEnd={() => setAnimate(false)} className={animate ? 'animate link' : 'link'}>
            <div className='logo-container'>
              <div className='logo'>
                  <Logo />
              </div>
            </div>
          </Link>
          <div className="settings">{}</div>
          <ul className="header__navlist">
            <li className="header__navitem">
              <Link to='/'>
                <button className="button__navlist">Главная</button>
              </Link>
            </li>
            <li className="header__navitem">
            {username ? 
                <Link to='/default'>
                  <button className="button__navlist">Мои подборки</button>
                </Link>
            :
                <Link to='/login'>
                  <button className="button__navlist">Мои подборки</button>
                </Link>
            }
            </li>
            <li className="header__navitem">
            {username ?
              <Link to='/'>
                <button className="button__navlist" onClick={logout}>Logout</button>
              </Link>
            :
              <Link to="/login">
                <button className="button__navlist">Login</button>
              </Link>
            }
            </li>
          </ul>
      </div>
      <div className="header__title">
        {handleTitle(setCategoryPath())}
      </div>
    </div>
  )
}

// ========================================

export default Header;
