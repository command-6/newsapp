import React from 'react';
import Sorce from '../sorce'

function Defcat1() {
        
        let categoryName = localStorage.getItem('openedCategory');

        return (
        <div>
                <div className='flex cat_title'>{categoryName}</div>
                <div className='flex category__list'>
                <Sorce sorce='https://dqv05.mocklab.io/Hi_News.ru/' delbutton='false'/>
                <Sorce sorce='https://dqv05.mocklab.io/TechCrunch/' delbutton='false'/>
                <Sorce sorce='https://dqv05.mocklab.io/MacRumors/' delbutton='false'/>
        </div>
        </div>
                )

}
export default Defcat1