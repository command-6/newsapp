import React from 'react';

import './index.css';



class AddSorceForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 'https://dqv05.mocklab.io/lenta/',
      massage: ''
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  handleSubmit(event) {
    event.preventDefault();
    let sorses = this.props.sorses;
    let source = this.state.value;
    let flag = true;

    sorses.forEach(element => {
      if (element === source) {
        flag = false;
        this.setState({ massage: 'Этот ресурс уже добавлен' })
      }
    })

    if (flag) {
      this.props.props(this.state.value);
    }
  }

  render() {
    return (
      <form className="form__container" onSubmit={this.handleSubmit}>
        <select className="select__form" name="sorceSelect" onChange={this.handleChange} value={this.state.value}>
          <option value='https://dqv05.mocklab.io/lenta/'>Lenta.ru</option>
          <option value='https://dqv05.mocklab.io/iz.ru/'>iz.ru</option>
          <option value='https://dqv05.mocklab.io/kommersant/'>КоммерсантЪ</option>
          <option value='https://dqv05.mocklab.io/Hi_News.ru/'>Hi-News.ru</option>
          <option value='https://dqv05.mocklab.io/TechCrunch/'>TechCrunch</option>
          <option value='https://dqv05.mocklab.io/MacRumors/'>MacRumors</option>
          <option value='https://dqv05.mocklab.io/habr/'>habr</option>
          <option value='https://dqv05.mocklab.io/jsfeeds/'>jsfeeds</option>
          <option value='https://dqv05.mocklab.io/request9/'>сайт 9</option>
          <option value='https://dqv05.mocklab.io/request10/'>сайт 10</option>
        </select>
        <button className="button__black">добавить</button>
        <div className="red">{this.state.massage}</div>
      </form>
    );
  }
}

export default AddSorceForm;
