import React from 'react';
import Sorce from '../sorce'

function Defcat2() {
        
        let categoryName = localStorage.getItem('openedCategory');

        return (
        <div>
                <div className='flex cat_title'>{categoryName}</div>
                <div className='flex category__list'>
                <Sorce sorce='https://dqv05.mocklab.io/lenta/' delbutton='false'/>
                <Sorce sorce='https://dqv05.mocklab.io/iz.ru/' delbutton='false'/>
                <Sorce sorce='https://dqv05.mocklab.io/kommersant/' delbutton='false'/>
                </div>
        </div>
        )

}
export default Defcat2