import React from 'react';
import './index.css';




class Sorce extends React.Component {
  constructor() {
    super();
    this.state = {
      error: null,
      isLoaded: false,
      sorceName: "",
      sorce:'',
      items: [],
      defaulpage: false,
    }
  }

  componentDidMount() {
    if(this.props.delbutton){
      this.setState({defaulpage: true})
    }
    fetch(this.props.sorce)
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            items: result.items,
            sorceName: result.name,
            sorce: this.props.sorce
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  render() {
    let classes='button__black-small' ;
    if (this.state.defaulpage){
      classes='button__black-small nodisplay'
    };
    const { error, isLoaded, items, sorceName } = this.state;
    if (error) {
      return <div>Ошибка</div>;
    } else if (!isLoaded) {
      return <div>Загрузка...</div>;
    } else {
      return (
        <div className = 'site__item'>
          <h3>{sorceName}</h3>
          <ul>
            {items.map(item => (
              <li key={item.id}>
                <a href={item.sorce} target="_blank" rel="noopener noreferrer">{item.name}</a>
              </li>
            ))}
          </ul>
          <button className={classes} onClick={() => {this.props.delete(this.state.sorce)}}>удалить</button>
        </div>
      );
    }
  }
}


// ========================================

export default Sorce;
