import React from 'react';
import Sorce from '../sorce'

function Defcat3() {
        let categoryName = localStorage.getItem('openedCategory');

        return (
                <div>
                        <div className='flex cat_title'>{categoryName}</div>
                        <div className='flex category__list'>
                        <Sorce sorce='https://dqv05.mocklab.io/habr/' delbutton='false'/>
                        <Sorce sorce='https://dqv05.mocklab.io/jsfeeds/' delbutton='false'/>
                        <Sorce sorce='https://dqv05.mocklab.io/request9/' delbutton='false'/>
                        </div>
                </div>
        )

}
export default Defcat3