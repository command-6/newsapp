import React from 'react';
import './index.css';
import { api } from '../Api/api'



class AddForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      description: '',
      message: ''
    };

    this.handleChange1 = this.handleChange1.bind(this);
    this.handleChange2 = this.handleChange2.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange1(event) {
    this.setState({ value: event.target.value });
  }

  handleChange2(event) {
    this.setState({ description: event.target.value });
  }
 
  handleSubmit(event) {
    let categories = api.getUserCategories();
    let flag = false;
    let value = this.state.value;
    if (categories) {
      categories.forEach(function (item) {
        if (item === value) {
          flag = true;
          return
        }
      });
      if (flag) {
        this.setState({message:  'такая категоря уже существует'});
      } else this.props.props(this.state.value, this.state.description);
    }

    event.preventDefault();
  }

  render() {
    return (
      <div className='form__container'>
        <form className="form__container" onSubmit={this.handleSubmit}>
          <label>
            <input minLength='1'  maxLength="20" className='input__form' name='category' type="text" value={this.state.value} onChange={this.handleChange1} placeholder="Имя категории" required/>
          </label>
          <label>
            <input maxLength="60"  className='input__form' name='description' type="text" value={this.state.description} onChange={this.handleChange2} placeholder="Добавьте описание" />
          </label>
          <button type='submit' className='button__black'>добавить</button>
          <div className='red'>{this.state.message}</div>
        </form>
        <button className='button__black back' onClick={this.props.back}>отмена</button>
      </div>
       
    );
  }
}

export default AddForm;