import {login, registration} from "./commands/loginCommands";

describe('Successful Login and registration', () => {

    beforeEach(() => {
        cy.restoreLocalStorage();
      });
      
      afterEach(() => {
        cy.saveLocalStorage();
      });
  
it('Successful registration', ()=> {
    cy.visit('https://newsapp-git-master.igor-polonskiy.vercel.app/')
    registration("pertseva", "test@mail.ru", "pertseva");
    cy.xpath("//div[@class='header__title']//div[@class='above'][contains(.,'pertseva')]").should('be.visible');
})

it('Successful login', ()=> {
    cy.xpath("//button[contains(text(), 'Logout')]").click();
    login("pertseva", "pertseva");
    cy.xpath("//div[@class='header__title']//div[@class='above'][contains(.,'pertseva')]").should('be.visible');
 })

})

describe('Unsuccessful Login and registration', () => {

  beforeEach(() => {
      cy.restoreLocalStorage();
    });
    
    afterEach(() => {
      cy.saveLocalStorage();
    });

it('Same login', ()=> {
  cy.xpath("//button[contains(text(), 'Logout')]").click();
  registration("pertseva", "test2@mail.ru", "pertseva");
  cy.get("div").contains("Данный пользователь уже зарегестрирован").should('be.visible');
})

it('Same email', ()=> {
  cy.get('input[name="username"]').clear().type("perrtseva")
  cy.get('input[name="email"]').clear().type("test@mail.ru")
  cy.get('input[name="password"]').clear().type("pertseva")
  cy.get("div").contains("Данный пользователь уже зарегестрирован").should('be.visible');
})

it('Login is too short', ()=> {
  cy.get('input[name="username"]').clear().type("123")
  cy.get('input[name="email"]').click();
  cy.xpath("//input[@name='username']/ancestor::div[@class='form-group']//div").contains("Минимальная длина 4 символа ").should('be.visible');
})

it('Password is too short', ()=> {
  cy.get('input[name="password"]').clear().type("123")
  cy.get('input[name="email"]').click();
  cy.xpath("//input[@name='password']/ancestor::div[@class='form-group']//div").contains("Минимальная длина 4 символа ").should('be.visible');
})

it('Wrong email', ()=> {
  cy.get('input[name="email"]').clear().type("test")
  cy.get('input[name="password"]').click();
  cy.get("div").contains("Введите корректный email").should('be.visible');
})




})