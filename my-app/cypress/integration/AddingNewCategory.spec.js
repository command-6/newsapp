import { addCategory, deleteCategory } from "./commands/categoriesCommands";
import { login, registration } from "./commands/loginCommands";
import { addSource, deleteSource } from "./commands/addSource";


describe('category manipulation', () => {

  beforeEach(() => {
    cy.visit('https://newsapp-git-master.igor-polonskiy.vercel.app/')
    registration("pertseva", "test@mail.ru", "pertseva");
    addCategory("Новости", "Новости");
  });

  afterEach(() => {

  });

  it('Registration and adding new category', () => {
    cy.get("div.category__title span").contains("Новости").should('be.visible');
    addCategory("Рисование", "Рисование");
    cy.get("div.category__title span").contains("Рисование").should('be.visible');
    deleteCategory("Рисование");
    cy.get("div").contains("Рисование").should('not.be.visible');
    cy.xpath("//div")
  })

  it('Open category and check category info', () => {
    cy.get("div.category__title span").contains("Новости").should('be.visible');
    cy.xpath("//span[contains(text(),'Новости')]").click();
    cy.get("div").contains("Новости").should('be.visible');
  })

  it('Open category, add source, go on the main page, go back and check that source is existing', () => {
    cy.xpath("//span[contains(text(),'Новости')]").click();
    addSource('Lenta.ru');
    cy.xpath("//button[contains(text(), 'Мои подборки')]").click();
    cy.xpath("//span[contains(text(),'Новости')]").click();
    cy.get(".site__item h3").contains('lenta.ru').should('be.visible');
  })

  it('Adding several sources and deleting them', () => {
    cy.get("div.category__title span").contains("Новости").should('be.visible');
    cy.xpath("//span[contains(text(),'Новости')]").click();
    addSource('Lenta.ru');
    cy.get(".button__container button").should('be.visible');
    cy.get(".site__item h3").contains('lenta.ru').should('be.visible');
    addSource('iz.ru');
    cy.get(".button__container button").should('be.visible');
    cy.get(".site__item h3").contains('iz.ru').should('be.visible');
    deleteSource('lenta.ru');
    cy.get(".site__item h3").contains('lenta.ru').should('not.be.visible');
    cy.get(".site__item h3").contains('iz.ru').should('be.visible');
    addSource('Lenta.ru');
    cy.get(".button__container button").should('be.visible');
  })

  it('Register, creating category, create source, logout and login', () => {
    cy.get("div.category__title span").contains("Новости").should('be.visible');
    cy.xpath("//span[contains(text(),'Новости')]").click();
    addSource('Lenta.ru');
    addSource('iz.ru');
    cy.saveLocalStorage();
    cy.xpath("//button[contains(text(), 'Logout')]").click();
    cy.restoreLocalStorage();
    login("pertseva","pertseva");
    cy.xpath("//span[contains(text(),'Новости')]").click();
    cy.get(".site__item h3").contains('iz.ru').should('be.visible');
    cy.get(".site__item h3").contains('lenta.ru').should('be.visible');

  })







})