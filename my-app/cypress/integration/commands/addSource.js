export const addSource = ('Add source', (sourceName) => {
    cy.get('.button__container button').click();
    cy.get('select').select(sourceName);
    cy.get('.button__black').click();
  })

  export const deleteSource = ('Delete source', (sourceName) => {
    cy.xpath('//div[@class="site__item"]/h3[contains(text(),"'+sourceName+'")]/ancestor::div[@class="site__item"]//button').click();

  })