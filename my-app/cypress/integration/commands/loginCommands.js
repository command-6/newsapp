export const login = ('Successful_login', (username, password) => {
    cy.xpath("//button[contains(text(), 'Login')]").click();
    cy.get('input[name="username"]').clear().type(username)
    cy.get('input[name="password"]').clear().type(password)
    cy.xpath('//div[@class="login-footer"]//button[contains(text(), "Login")]').click()
  })


export const registration = ('Successful_registration', (username, email, password) => {
    cy.xpath("//button[contains(text(), 'Login')]").click();
    cy.get('.login .text').click();
    cy.get('input[name="username"]').clear().type(username)
    cy.get('input[name="email"]').clear().type(email)
    cy.get('input[name="password"]').clear().type(password)
    cy.xpath('//button[contains(text(), "Register")]').click();
  })