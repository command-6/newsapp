export const addCategory = ('Add category', (categoryName, categoryDescription) => {
    cy.xpath('//span[contains(text(), "Добавить")]').click();
    cy.get('input[name="category"]').clear().type(categoryName);
    cy.get('input[name="description"]').clear().type(categoryDescription);
    cy.xpath('//button[contains(text(), "добавить")]').click();
  })

  export const deleteCategory = ('Delete category', (categoryName) => {
    cy.xpath('//div[contains(text(),"'+categoryName+'")]/ancestor::div[@class="category__item"]').trigger('mouseover')
    cy.xpath('//div[contains(text(),"'+categoryName+'")]/ancestor::div[@class="category__item"]//button').click();
  })