const express = require('express');
const path = require('path');
const app = express();

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, HEAD, OPTIONS, POST, PUT');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
  
    next();
  });

app.get('/lenta/', (req, res) => {
    res.sendFile(path.resolve(__dirname, './lenta.json'), )
});
app.get('/iz.ru/', (req, res) => {
    res.sendFile(path.resolve(__dirname, './iz.ru.json'), )
});
app.get('/Kommersant/', (req, res) => {
    res.sendFile(path.resolve(__dirname, './kommersant.json'), )
});
app.get('/Hi_News.ru/', (req, res) => {
    res.sendFile(path.resolve(__dirname, './Hi_News.ru.json'), )
});
app.get('/request5/', (req, res) => {
    res.sendFile(path.resolve(__dirname, './response5.json'), )
});
app.get('/request6/', (req, res) => {
    res.sendFile(path.resolve(__dirname, './response6.json'), )
});
app.get('/request7/', (req, res) => {
    res.sendFile(path.resolve(__dirname, './response7.json'), )
});
app.get('/request8/', (req, res) => {
    res.sendFile(path.resolve(__dirname, './response8.json'), )
});
app.get('/request9/', (req, res) => {
    res.sendFile(path.resolve(__dirname, './response9.json'), )
});
app.get('/request10/', (req, res) => {
    res.sendFile(path.resolve(__dirname, './response10.json'), )
});

app.get('/', (req, res) =>{
    res.status(200).json({
        massege: "Working",
    })
})

app.listen(4040, ()=> console.log('server started'))