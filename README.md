# SourceBox

Это прототип приложения для хранения подборок новостей.

Чтобы запустить приложение надо открыть консоль зайти в папку my-app
и выполнить npm start, в браузере откроется наше приложение или
просто можно пройти по ссылке https://newsapp-flax.vercel.app/

На главной странице собраны подборки от разаработчиков.
Для полноценного использования следует зарегистрироваться,
тогда будет доступен весь функционал: создание собственных подборок,
их удаление и редактирование, а так же возможность расположить их в удобном
порядке на экране.


# Тестирование

Для тестирования был выбран: Cypress (https://www.cypress.io/)

    Установка:

    1. Чтобы установить Cypress введите команду:
    npm install cypress --save-dev
    2. Чтобы установить cypress-xpath (https://github.com/cypress-io/cypress-xpath) введите команду:
    npm install -D cypress-xpath
    3. Чтобы открыть Cypress введите команду:
    npm run cypress:open
    4. Установите репортер (https://docs.cypress.io/guides/tooling/reporters.html#Examples):
    npm install --save-dev cypress-multi-reporters mocha-junit-reporter
    npm install --save-dev mochawesome mochawesome-merge mochawesome-report-generator
    5. Генерация отчета:
    cypress run --reporter mochawesome \ --reporter-options reportDir=reporter-config.json,overwrite=false,html=false,json=true
    npx mochawesome-merge "cypress/results/*.json" > mochawesome.json
    npx marge mochawesome.json

    Дополнительная информация:
    Stop using Page Objects and Start using App Actions - https://www.cypress.io/blog/2019/01/03/stop-using-page-objects-and-start-using-app-actions/
    

    
